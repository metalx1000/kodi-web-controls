#!/bin/sh
echo "Content-type: text/plain"
echo "Access-Control-Allow-Origin: *\n"

curl 'http://localhost:80/jsonrpc?awx' -H 'Origin: http://192.168.1.146' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36' -H 'Content-Type: application/json' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Referer: http://192.168.1.146/' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --data-binary '{"jsonrpc":"2.0","id":"OPProp","method":"Player.GetProperties","params":{ "playerid": 1,"properties":["speed", "shuffled", "repeat", "subtitleenabled", "time", "totaltime", "position", "currentaudiostream", "partymode"] } }' --compressed 
